
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <errno.h>   /* ERROR Number Definitions           */
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "rs232.h"
#include <sys/sendfile.h>
#include <limits.h> /* PATH_MAX */
#include <sys/mman.h>
#include <sys/epoll.h>
#include <time.h>

#define BUF_LEN 1024*1024*20
#define MAX_EVENTS 32
#define CHUNK_SIZE 63 //63 = MAX full speed USB packet size(64) - s_crc8(1)
typedef struct { 
    uint8_t s_crc8;
    uint8_t array[CHUNK_SIZE];
} packet;

enum push {
    WAITING = 0,
    START = 2,
    READY = 3
};
uint8_t calculate_crc8(uint8_t* data, size_t len);
static void epoll_ctl_add(int epfd, int fd, uint32_t events);
packet PACKET = {0};
packet RECEIVED_PACKET = {0};
double time1, timedif;
int main(int argc, char **argv)
{
    static uint8_t received_buf[BUF_LEN];
    char * buf;
    buf = malloc(BUF_LEN);
    unsigned int i;
    unsigned int n;
    int epfd;
    int nfds;
    char ch = '0';
    uint8_t crc;
    struct timeval begin, end;
    enum push push_state = WAITING;
    uint8_t crc8;
    struct epoll_event events[MAX_EVENTS];
    struct stat stat_buf;
    const char *fromfile = argv[1];
    const char *tofile = argv[2];
    int tofd;
    if ( argc != 3 ) {
        printf("usage: %s <filepath/filename> <filepath/newfilename>\n", argv[0]);
        exit(-1);
    }

    int fromfd = open(fromfile, O_RDONLY, S_IRUSR | S_IWUSR);
    if ( fromfd == -1) {
        perror("open");
        exit(-1);
    }
    if (fstat( fromfd, &stat_buf) == -1) {
        printf("Bad file\n");
    }
    else {
        printf("File size : %ld\n", stat_buf.st_size);
    }
    unsigned int last_chunk = stat_buf.st_size % CHUNK_SIZE;
    unsigned int chunk_amount = (((stat_buf.st_size-last_chunk)/CHUNK_SIZE)-1);
    uint8_t chunks[chunk_amount][CHUNK_SIZE];
    unsigned int k = 0;
    unsigned int s = 0;
    uint8_t *file_in_memory = mmap(NULL, stat_buf.st_size,PROT_READ | PROT_WRITE, MAP_PRIVATE, fromfd, 0);
    uint8_t received_crc8;
    struct conn *c = set_rs232();
    unsigned int f;
    unsigned int j;
    size_t total_bytes = 0;
    unsigned int chunking_place;
    printf("Press C to send the selected file over UART\n");
    epfd = epoll_create(1);
    if (epfd == -1) {
        printf("Faild to create epoll file descriptor\n");
        return 1;
    }

    epoll_ctl_add(epfd ,c->socket , EPOLLIN | EPOLLOUT | EPOLLET);
    while (1) {
        int event_count = epoll_wait(epfd, events, MAX_EVENTS, 1000);
        chunking_place = (f * CHUNK_SIZE);

        if ( (push_state == WAITING) && (events[0].events & EPOLLOUT)) {
            if ((read(STDIN_FILENO, buf, sizeof(buf)) > 0)) {

                if ((buf[0] == 'C')){

                    gettimeofday(&begin, 0);
                    time1 = (double) clock();     //get initial time
                    time1 = time1 / CLOCKS_PER_SEC;
                    push_state = READY; 
                    tofd = open(tofile, O_WRONLY | O_CREAT,0664, stat_buf.st_mode);
                    char filepath[PATH_MAX];
                    char *res = realpath(argv[2], filepath);

                    if (res) {

                        printf("File location : %s\n",filepath );
                    } else {
                        perror("realpath");
                        exit(EXIT_FAILURE);
                    }


                    if ( tofd == -1) {
                        perror("open");
                        exit(-1);
                    }
                }else {
                    printf("Press C to start transfere or [Ctrl + x] to exit.\n");
                    push_state == WAITING;
                }


            }


        }

        if (push_state == READY) {


            memcpy(&PACKET.array[0], &file_in_memory[chunking_place], CHUNK_SIZE);
            PACKET.s_crc8 = calculate_crc8(PACKET.array, CHUNK_SIZE);
            j = write(events[0].data.fd, &PACKET, sizeof(PACKET) ); 
            if (j == -1) {

                printf("Error:  %i\n", errno);
            } else {
                s = s + j;

            }

            size_t bytes_read = 0;
            memset(received_buf,0, sizeof(received_buf));

            while (total_bytes != s) {
                bytes_read  = read(events[0].data.fd, received_buf,sizeof(received_buf));
                if( bytes_read == -1) {

                    printf("Error:  %i\n", errno);

                } else {

                }
                if (bytes_read > 0) {
                    total_bytes = total_bytes + bytes_read;

                }
            }

            uint16_t z;
            if ( f == (chunk_amount+1  ) ) {
                if (push_state == READY) {
                    memset(RECEIVED_PACKET.array, 0, sizeof(RECEIVED_PACKET.array));
                    memcpy(&RECEIVED_PACKET.array[0], &received_buf[1], last_chunk);
                    RECEIVED_PACKET.s_crc8 = received_buf[0];
                    crc8 = calculate_crc8(RECEIVED_PACKET.array, CHUNK_SIZE);
                    if (crc8 == RECEIVED_PACKET.s_crc8) {

                        printf("Packets %d / Received %.2ldKB                \n",f, total_bytes/(1024) ); 
                        write(tofd, RECEIVED_PACKET.array, last_chunk);
                        gettimeofday(&end, 0);
                        long seconds = end.tv_sec - begin.tv_sec;
                        long microseconds = end.tv_usec - begin.tv_usec;
                        double elapsed = seconds + microseconds*1e-6;
                        timedif = (((double) clock()) / CLOCKS_PER_SEC) - time1;
                        printf("CPU time in seconds - %f\n", timedif);
                        printf("WALL time in seconds - %.3f\n", elapsed);
                        printf("Speed - %.6f Mbit/s\n", (stat_buf.st_size/elapsed)/(1024*1024));
                        close(fromfd);
                        close(tofd);
                        close(c->socket);

                        printf("EXIT\n"); 
                        exit(0);


                    } else {

                        printf("At packet# [%d] crs8 value [%x] do not match\n",f,  crc8);
                    }

                }
            } else {


                memset(RECEIVED_PACKET.array, 0, sizeof(RECEIVED_PACKET.array));

                memcpy(&RECEIVED_PACKET.array[0], &received_buf[1], sizeof(RECEIVED_PACKET.array));
                RECEIVED_PACKET.s_crc8 = received_buf[0];
                crc8 = calculate_crc8(RECEIVED_PACKET.array, CHUNK_SIZE);
                if (crc8 == RECEIVED_PACKET.s_crc8) {

                    f++;

                } else {

                    printf("At packet# [%d] crs8 value [%x] do not match\n",f,  crc8);
                }
                push_state = READY;
                printf("Packet#: %d / Received %ldKB\r",f,total_bytes/1024 ); 
                write(tofd, RECEIVED_PACKET.array, sizeof(RECEIVED_PACKET.array));
            }
        }
        }
        free(buf);
        close(fromfd);
        close(tofd);
        close(c->socket);
        exit(0);
    }


    const uint8_t crc8x_table[256] = {
        0x00,0x31,0x62,0x53,0xC4,0xF5,0xA6,0x97,0xB9,0x88,0xDB,0xEA,0x7D,0x4C,0x1F,0x2E,
        0x43,0x72,0x21,0x10,0x87,0xB6,0xE5,0xD4,0xFA,0xCB,0x98,0xA9,0x3E,0x0F,0x5C,0x6D,
        0x86,0xB7,0xE4,0xD5,0x42,0x73,0x20,0x11,0x3F,0x0E,0x5D,0x6C,0xFB,0xCA,0x99,0xA8,
        0xC5,0xF4,0xA7,0x96,0x01,0x30,0x63,0x52,0x7C,0x4D,0x1E,0x2F,0xB8,0x89,0xDA,0xEB,
        0x3D,0x0C,0x5F,0x6E,0xF9,0xC8,0x9B,0xAA,0x84,0xB5,0xE6,0xD7,0x40,0x71,0x22,0x13,
        0x7E,0x4F,0x1C,0x2D,0xBA,0x8B,0xD8,0xE9,0xC7,0xF6,0xA5,0x94,0x03,0x32,0x61,0x50,
        0xBB,0x8A,0xD9,0xE8,0x7F,0x4E,0x1D,0x2C,0x02,0x33,0x60,0x51,0xC6,0xF7,0xA4,0x95,
        0xF8,0xC9,0x9A,0xAB,0x3C,0x0D,0x5E,0x6F,0x41,0x70,0x23,0x12,0x85,0xB4,0xE7,0xD6,
        0x7A,0x4B,0x18,0x29,0xBE,0x8F,0xDC,0xED,0xC3,0xF2,0xA1,0x90,0x07,0x36,0x65,0x54,
        0x39,0x08,0x5B,0x6A,0xFD,0xCC,0x9F,0xAE,0x80,0xB1,0xE2,0xD3,0x44,0x75,0x26,0x17,
        0xFC,0xCD,0x9E,0xAF,0x38,0x09,0x5A,0x6B,0x45,0x74,0x27,0x16,0x81,0xB0,0xE3,0xD2,
        0xBF,0x8E,0xDD,0xEC,0x7B,0x4A,0x19,0x28,0x06,0x37,0x64,0x55,0xC2,0xF3,0xA0,0x91,
        0x47,0x76,0x25,0x14,0x83,0xB2,0xE1,0xD0,0xFE,0xCF,0x9C,0xAD,0x3A,0x0B,0x58,0x69,
        0x04,0x35,0x66,0x57,0xC0,0xF1,0xA2,0x93,0xBD,0x8C,0xDF,0xEE,0x79,0x48,0x1B,0x2A,
        0xC1,0xF0,0xA3,0x92,0x05,0x34,0x67,0x56,0x78,0x49,0x1A,0x2B,0xBC,0x8D,0xDE,0xEF,
        0x82,0xB3,0xE0,0xD1,0x46,0x77,0x24,0x15,0x3B,0x0A,0x59,0x68,0xFF,0xCE,0x9D,0xAC };

    uint8_t calculate_crc8(uint8_t* data, size_t len) {
        uint8_t crc = 0xFF; // init value
        for (size_t i = 0; i < len; i++) {
            crc = crc8x_table[data[i] ^ crc];
        }
        return crc;
    }

    static void epoll_ctl_add(int epfd, int fd, uint32_t events) {
        struct epoll_event ev;
        ev.events = events;
        ev.data.fd = fd;
        if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
            perror("epoll_ctl()\n");
            exit(1);
        }
    }




