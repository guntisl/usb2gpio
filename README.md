#Prerequisites:

you will need:
CMake v3.15+ - found at https://cmake.org/
C/C++ Compiler

#Installing

git clone https://gitlab.com/guntisl/usb2gpio.git

cd epoll_host && mkdir build

cmake -S ./ -B ./build

cd build && make


#Usage

./epoll_host <Source-file-path/Source-filename> <Destination-filepath/Destination-file-name>


#References

sendfile example
https://medium.com/swlh/linux-zero-copy-using-sendfile-75d2eb56b39b

epoll server example
https://github.com/onestraw/epoll-example/tree/master

rs232 uart socket example
https://github.com/5peedmanual/serial-communication-rs232/tree/master

CRC example
https://www.scaler.com/topics/crc-program-in-c/
https://stackoverflow.com/questions/51752284/how-to-calculate-crc8-in-c?rq=3

Simple Serial Framing
https://jgtao.me/content/04-02-16/
